#  Using Enum Values in SwiftData Predicates

A sample app showing one method of dealing with the current limitations around using SwiftData with enum types in the Model.

See [My Blog Post on this](https://pmcconnell.micro.blog)

