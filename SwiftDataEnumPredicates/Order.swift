//
//  Order.swift
//  SwiftDataEnumPredicates
//
//  Created by Patrick McConnell on 4/17/24.
//

import SwiftData

@Model
class Order {
    var color: ItemColor
    var colorString: String
    var size: ItemSize
    var price: Double

    var stringColor: String {
        color.rawValue
    }

    init(color: ItemColor, size: ItemSize, price: Double) {
        self.color = color
        self.colorString = color.rawValue
        self.size = size
        self.price = price
    }
}

enum ItemColor: String, CaseIterable, Identifiable, Codable {
    case red
    case blue
    case green

    var id: Self {self}
}

enum ItemSize: Int, CaseIterable, Identifiable, Codable {
    case small
    case medium
    case large
    case extraLarge

    var id: Self {self}

    var shortString: String {
        switch self {
            case .small:
                return "S"
            case .medium:
                return "M"
            case .large:
                return "L"
            case .extraLarge:
                return "XL"
        }
    }
}
