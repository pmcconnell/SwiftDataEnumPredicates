//
//  ContentView.swift
//  SwiftDataEnumPredicates
//
//  Created by Patrick McConnell on 4/17/24.
//

import SwiftUI
import SwiftData

struct ContentView: View {
    @Environment(\.modelContext) var modelContext
    @Query var orders: [Order]

    init(color: ItemColor? = nil) {
        if let color {
            _orders = Query(filter: #Predicate<Order> { order in
                order.colorString.starts(with: color.rawValue)
            })
        }
    }

    var body: some View {
        List(orders) {order in
            HStack {
                Text(order.color.rawValue.capitalized)
                Text(order.size.shortString)
                Text(order.price, format: .currency(code: "USD").precision(.fractionLength(2)))
            }
        }
    }
}

#Preview {
    do {
        let config = ModelConfiguration(isStoredInMemoryOnly: true)
        let container = try ModelContainer(for: Order.self, configurations: config)
        container.mainContext.insert(Order(color: .red, size: .medium, price: 12.0))
        container.mainContext.insert(Order(color: .green, size: .large, price: 10.0))
        container.mainContext.insert(Order(color: .red, size: .extraLarge, price: 14.0))
        container.mainContext.insert(Order(color: .blue, size: .small, price: 12.0))

        return ContentView(color: .red)
            .modelContainer(container)
    } catch {
        return Text("error creating model container: \(error.localizedDescription)")
    }

}
