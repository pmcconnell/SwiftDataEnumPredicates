//
//  SwiftDataEnumPredicatesApp.swift
//  SwiftDataEnumPredicates
//
//  Created by Patrick McConnell on 4/17/24.
//

import SwiftUI
import SwiftData

@main
struct SwiftDataEnumPredicatesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
        .modelContainer(for: Order.self)
    }
}
